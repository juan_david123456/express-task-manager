// Importar express
const express = require("express");
// Importar mongoose
const mongoose = require("mongoose");
//Importar body.parser  
const bodyParser = require("body-parser");
require("dotenv/config");
const usuariosRouter = require("./routes/usuarios")
// Rutas
const tareasRouter = require('./routes/tareas')

const bookmarksRouter = require('./routes/bookmarks')
// Creación de la aplicación
const app = express();
//
const cors = require('cors')

//middleware
app.use(cors());
app.use(bodyParser.json());
app.use("/usuarios", usuariosRouter);
app.use('/tareas', tareasRouter);
app.use('/bookmarks', bookmarksRouter);


// Rutas
app.get("/", (req, res) => {
  res.send("Ejemplo clase");
});

//mongodb+srv://ejemplo_clase:<ejemploclase>@cluster0.7cbze.mongodb.net/<dbname>?retryWrites=true&w=majority

	
mongoose.connect(process.env.CONEXION_DB,
  { useUnifiedTopology: true, useNewUrlParser: true },
  () => {
    console.log("Conectado a la base de datos...");
  }
);

// app.get("/tareas", (req, res) => {
//   res.send("Estamos en tareas");
// });

// Inicio del servidor
app.listen(3000);