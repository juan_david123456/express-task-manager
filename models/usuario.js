const { net } = require("electron");
const mongoose = require("mongoose");
const bcrypt = require('bcrypt');

const UsuarioSchema = new   mongoose.Schema({
    user: {
        type: String,
        required: true,
        unique:true,
    },
    password: {
        type: String,
        required: true,
    },
    nombre: {
        type: String,
        required: true
    },
    correo: {
        type: String,
        required: true
    },
    fecha: {
        type: Date,
        default: Date.now
    }

});     

UsuarioSchema.pre('save', function(next){
    if (this.isNew || this.isModified('password')){
        const document = this;
        bcrypt.hash(document.password, 10, (error, hashedPassword) =>{
            if(error){
                next(error);
            }
            document.password = hashedPassword;
            next();
        });
        
    }else{
            next();
    }   
})

UsuarioSchema.methods.isCorrectPassword = function(password, cb){
    bcrypt.compare(password, this.password, function(err, same){
        if(err){
            return cb(err);
        }
        cd (null, same)
    });
}


// Exportar el esquema
module.exports = mongoose.model("Usuario", UsuarioSchema);