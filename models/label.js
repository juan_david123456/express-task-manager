const mongoose = require("mongoose");

const LabelSchema = mongoose.Schema({
  Label: {
    type: String,
    required: true,
  },
});

// Exportar el esquema
module.exports = mongoose.model("Label" , LabelSchema);
