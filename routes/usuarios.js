const express = require("express");
const Usuario = require("../models/usuario");
const { use } = require("./tareas");

// Crea el router de tareas
const router = express.Router();

router.post("/", async (req, res) => {

  try {
    // Crea el objeto
    const nuevoUsuario = new Usuario({

      user: req.body.user,
      password: req.body.password,
      nombre: req.body.nombre,
      correo: req.body.correo,
    });
    // Guarda el objeto en BD
    let resultado = await nuevoUsuario.save();
    // Retorna el objeto guardado al cliente
    res.json(resultado);
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

router.get("/:user", async (req, res) => {
  let user = req.params.user;
  // let password = req.params.password;
    try {
      // Obtiene una tarea por su id
      
      const tarea = await Usuario.findOne({ user: user});
      // retorna la respuesta al cliente
      res.json(tarea);
      res.json(password)
    } catch (error) {
      // Retorna el error al cliente
      res.status(500).send(error);
    }
});

router.get("/", async (req, res) => {
  try {
    // Obtiene todas las tareas
    const tareas = await Usuario.find();

    // retorna la respuesta al cliente
    res.json(tareas);
  } catch (error) {
    // Retorna el error al cliente
    res.status(500).send(error);
  }
});

module.exports = router;
