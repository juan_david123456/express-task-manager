const express = require("express");

// Creación del router
const router = express.Router();

const Tarea = require("../models/tarea");

// // Creación de ruta
// router.get("/", (req, res) => {
//   res.send("TAREAS");
// });

router.get("/1", (req, res) => {
  res.send("TODAS LAS TAREAS");
});

router.get("/2", (req, res) => {
  res.send("UNA SOLA TAREA");
});

// Metodo get id	
router.get("/:titulo", async (req, res) => {
  let titulo = req.params.titulo;
  try {
    // Obtiene una tarea por su id
    const tarea = await Tarea.findOne({titulo:titulo});
    // retorna la respuesta al cliente
    res.json(tarea);
  } catch (error) {
    // Retorna el error al cliente
    res.status(500).send(error);
  }
});

//Metodo get 1
router.get("/", async (req, res) => {
  try {
    // Obtiene todas las tareas
    const tareas = await Tarea.find();

    // retorna la respuesta al cliente
    res.json(tareas);
  } catch (error) {
    // Retorna el error al cliente
    res.status(500).send(error);
  }
});


//Metodo Post
router.post("/", async (req, res) => {
  try {
    // Crea el objeto
    const tarea = new Tarea({
      titulo: req.body.titulo,
      descripcion: req.body.descripcion,
      fecha: req.body.fecha
    });
    // Guarda el objeto en la base de datos
    let resultado = await tarea.save();
    // Retorna la respuesta al cliente
    res.json(resultado);
  } catch (error) {
    // Retorna el error al cliente
    res.status(500).send(error);
  }
});



//Eliminar

router.delete("/:id", async (req, res) => {
  let id = req.params.id;
  try {
    // Elimina la tarea dado su id
    const resultado = await Tarea.remove({ _id: id });
    // Retorna la respuesta al cliente
    res.json(resultado);
  } catch (error) {
    // Retorna el error al cliente
    res.status(500).send(error);
  }
});

// Exportar el router
module.exports = router;